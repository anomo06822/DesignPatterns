using System;

namespace DesignPatterns.FacadePattern
{
    public class PopcornPopper
    {
        public void Pop()
        {
            System.Console.WriteLine("Popcorn popper pop");
        }

        public void On()
        {
            System.Console.WriteLine("Popcorn popper on");
        }

        public void Off()
        {
            System.Console.WriteLine("Popcorn popper off");
        }
    }
}