using System;

namespace DesignPatterns.FacadePattern
{
    public class TheaterLights
    {
        public void dim(int v)
        {
            System.Console.WriteLine(string.Format("TheaterLights dim {0}", v));
        }

        public void On()
        {
            System.Console.WriteLine("TheaterLights on");
        }
    }
}