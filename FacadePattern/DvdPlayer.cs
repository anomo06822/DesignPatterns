using System;

namespace DesignPatterns.FacadePattern
{
    public class DvdPlayer
    {
        public void On()
        {
            System.Console.WriteLine("DvdPlayer on");
        }

        public void Play(string movie)
        {
            System.Console.WriteLine(string.Format("DvdPlayer : {0} play", movie));
        }

        public void Stop()
        {
            System.Console.WriteLine("DvdPlayer stop");
        }

        public void Eject()
        {
            System.Console.WriteLine("DvdPlayer eject");
        }

        public void Off()
        {
            System.Console.WriteLine("DvdPlayer off");
        }
    }
}