using System;

namespace DesignPatterns.FacadePattern
{
    public class Projector
    {
        public void On()
        {
            System.Console.WriteLine("projector on");
        }

        public void wideScreenMode()
        {
            System.Console.WriteLine("projector wide screen mode");
        }

        public void Off()
        {
            System.Console.WriteLine("projector off");
        }
    }
}