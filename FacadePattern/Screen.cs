using System;

namespace DesignPatterns.FacadePattern
{
    public class Screen
    {
        public void down()
        {
            System.Console.WriteLine("screen down");
        }

        public void Up()
        {
            System.Console.WriteLine("screen up");
        }
    }
}