using System;

namespace DesignPatterns.FacadePattern
{
    public class Amplifier
    {
        public DvdPlayer dvd;

        public void On()
        {
            System.Console.WriteLine("Amplifer on");
        }

        public void SetDvd(DvdPlayer dvd)
        {
            System.Console.WriteLine("Amplifer set dvd");
            dvd = this.dvd;
        }

        public void SetSurroundSound()
        {
            System.Console.WriteLine("Amplifer set surround sound");
        }

        public void Off()
        {
            System.Console.WriteLine("Amplifer off");
        }
    }
}