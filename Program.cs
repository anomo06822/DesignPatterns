﻿using System;
using DesignPatterns.ObserverPattern;
using DesignPatterns.DecoratorPattern;
using DesignPatterns.FactoryPattern.SimpleFactory;
using DesignPatterns.FactoryPattern.FactoryMethod;
using DesignPatterns.FactoryPattern.AbstractFactory;
using DesignPatterns.SingletonPattern;
using DesignPatterns.CommandPattern;
using DesignPatterns.AdapterPattern;
using DesignPatterns.FacadePattern;

namespace DesignPatterns
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            ///1.觀察者模式
            // new ObserverPatternDemo().Start();

            ///2.裝飾者模式
            // new DecoratorPatternDemo().Start();

            ///3.工廠模式
            // SimpleFactory
            // new SimpleFactoryDemo().Start();
            // FactoryMethod
            //new FactoryMethodDemo().Start();
            // AbstractFactory
            //new AbstractFactoryDemo().Start();

            ///4.獨體模式
            //new SingletonDemo().Start();

            ///5.命令模式
            //new SimpleRemoteControlDemo().Start();

            ///6.轉接器模式與表象模式
            //Adapter
            //new AdapterPatternDemo().Start();
            //Facade
            new FacadePatternDemo().Start();
        }
    }
}
