namespace DesignPatterns.FactoryPattern.FactoryMethod
{
    public class ChicagoStyleCheesePizza : Pizza
        {   
        public ChicagoStyleCheesePizza()
        {
            name = "Chicago Style Deep Dish Cheese Pizza";
            dough = "Extra Thick Crust Dough";
            sauce = "Plumm Tomato Sauce";

            toppings.Add("Shredded Mozzarella Cheese");
        }

        public override void Cut()
        {
            System.Console.WriteLine("Cutting the pizza into square slices");
        }
    }
}