namespace DesignPatterns.FactoryPattern.FactoryMethod
{
    public class FactoryMethodDemo
    {
        public void Start()
        {
            PizzaStore nyStore = new NYPizzaStore();
            PizzaStore chicagoStore = new ChicagoPizzaStore();

            Pizza pizza = nyStore.OrderPizza("Cheese");
            System.Console.WriteLine("Ethan ordered a " + pizza.getName() + "\n");

            System.Console.WriteLine("------------------------------------------------");

            pizza = chicagoStore.OrderPizza("Cheese");
            System.Console.WriteLine("Ethan ordered a " + pizza.getName() + "\n");
        }
    }
}