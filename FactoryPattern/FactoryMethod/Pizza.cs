using System.Collections;

namespace DesignPatterns.FactoryPattern.FactoryMethod
{
    public abstract class Pizza
    {
        public string name;
        public string dough;
        public string sauce;

        public ArrayList toppings = new ArrayList();

        public virtual void Prepare()
        {
            System.Console.WriteLine("Prepareing " + name);
            System.Console.WriteLine("Tossing dough...");
            System.Console.WriteLine("Adding sauce...");
            System.Console.WriteLine("Adding toppings: ");
            for (int i = 0; i < toppings.Count; i++)
            {
                System.Console.WriteLine("  " + toppings[i]);
            }
        }

        public virtual void Bake()
        {
            System.Console.WriteLine("Bake for 25 minutes at 350");
        }

        public virtual void Cut()
        {
            System.Console.WriteLine("Cutting the pizza into diagonal slices");
        }

        public virtual void Box()
        {
            System.Console.WriteLine("Place pizza in official PizzaStore box");
        }

        public string getName()
        {
            return name;
        }
    }
}