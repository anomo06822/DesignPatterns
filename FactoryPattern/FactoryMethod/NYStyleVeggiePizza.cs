namespace DesignPatterns.FactoryPattern.FactoryMethod
{
    public class NYStyleVeggiePizza : Pizza
    {
        public NYStyleVeggiePizza(){
            name = "NY Style Sauce and Veggie Pizza";
            dough = "Thin Crust Dough";
            sauce = "Marinara Sauce";

            toppings.Add("Grated Reggiano Veggie");
        }
    }
}