namespace DesignPatterns.FactoryPattern.FactoryMethod
{
    public class NYPizzaStore : PizzaStore
    {
        protected override Pizza CreatePizza(string item)
        {
            if(item.Equals("Cheese"))
            {
                return new NYStyleCheesePizza();
            }
            else if(item.Equals("Veggie"))
            {
                return new NYStyleVeggiePizza();
            }
            else
            {
                return null;
            }
        }
    }
}