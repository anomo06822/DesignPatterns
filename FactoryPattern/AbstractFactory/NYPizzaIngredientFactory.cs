using DesignPatterns.FactoryPattern.AbstractFactory.Ingredient.Cheese;
using DesignPatterns.FactoryPattern.AbstractFactory.Ingredient.Clam;
using DesignPatterns.FactoryPattern.AbstractFactory.Ingredient.Dough;
using DesignPatterns.FactoryPattern.AbstractFactory.Ingredient.Pepperoni;
using DesignPatterns.FactoryPattern.AbstractFactory.Ingredient.Sauce;
using DesignPatterns.FactoryPattern.AbstractFactory.Ingredient.Veggie;

namespace DesignPatterns.FactoryPattern.AbstractFactory
{
    public class NYPizzaIngredientFactory : PizzaIngredientFactory
    {
        public Dough CreateDough()
        {
            return new Dough();
        }
        public Sauce CreateSauce()
        {
            return new Sauce();
        }
        public Cheese CreateCheese()
        {
            return new Cheese();
        }
        public Veggies[] CreateVeggies()
        {
            Veggies[] veggies = new Veggies[]{ new Garlic(), new Onion(), new Mushroom(), new RedPepper() };
            return veggies;
        }
        public Pepperoni CreatePepperoni()
        {
            return new SlicedPepperoni();
        }
        public Clams CreateClam() 
        {
            return new FreshClams();
        }       
    }
}