namespace DesignPatterns.FactoryPattern.AbstractFactory.Ingredient.Dough
{
    public class Dough
    {
        public Dough()
        {
            System.Console.WriteLine("Dough");
        }
    }
}