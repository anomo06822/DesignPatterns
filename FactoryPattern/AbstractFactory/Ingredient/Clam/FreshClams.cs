namespace DesignPatterns.FactoryPattern.AbstractFactory.Ingredient.Clam
{
    internal class FreshClams : Clams
    {
        public FreshClams()
        {
            System.Console.WriteLine("FreshClams");
        }
    }
}