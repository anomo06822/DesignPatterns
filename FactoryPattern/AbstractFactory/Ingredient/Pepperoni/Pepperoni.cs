namespace DesignPatterns.FactoryPattern.AbstractFactory.Ingredient.Pepperoni
{
    public class Pepperoni
    {
        public Pepperoni()
        {
            System.Console.WriteLine("Pepperoni");
        }
    }
}