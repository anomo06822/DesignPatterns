namespace DesignPatterns.FactoryPattern.AbstractFactory.Ingredient.Pepperoni
{
    internal class SlicedPepperoni : Pepperoni
    {
        public SlicedPepperoni()
        {
            System.Console.WriteLine("SlicedPepperoni");
        }
    }
}