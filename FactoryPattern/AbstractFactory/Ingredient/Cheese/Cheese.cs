namespace DesignPatterns.FactoryPattern.AbstractFactory.Ingredient.Cheese
{
    public class Cheese
    {
        public Cheese()
        {
            System.Console.WriteLine("Cheese");
        }
    }
}