namespace DesignPatterns.FactoryPattern.AbstractFactory.Ingredient.Sauce
{
    public class Sauce
    {
        public Sauce()
        {
            System.Console.WriteLine("Sauce");
        }
    }
}