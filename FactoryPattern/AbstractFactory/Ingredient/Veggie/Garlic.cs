namespace DesignPatterns.FactoryPattern.AbstractFactory.Ingredient.Veggie
{
    public class Garlic : Veggies
    {
        public Garlic()
        {
            System.Console.WriteLine("Garlic");
        }
    }
}