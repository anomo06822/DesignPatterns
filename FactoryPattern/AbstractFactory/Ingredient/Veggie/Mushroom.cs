namespace DesignPatterns.FactoryPattern.AbstractFactory.Ingredient.Veggie
{
    public class Mushroom : Veggies
    {
        public Mushroom()
        {
            System.Console.WriteLine("Mushroom");
        }
    }
}