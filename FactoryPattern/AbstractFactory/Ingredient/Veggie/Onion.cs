namespace DesignPatterns.FactoryPattern.AbstractFactory.Ingredient.Veggie
{
    public class Onion : Veggies
    {
        public Onion()
        {
            System.Console.WriteLine("Onion");
        }
    }
}