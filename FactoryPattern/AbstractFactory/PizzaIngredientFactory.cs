using DesignPatterns.FactoryPattern.AbstractFactory.Ingredient.Cheese;
using DesignPatterns.FactoryPattern.AbstractFactory.Ingredient.Clam;
using DesignPatterns.FactoryPattern.AbstractFactory.Ingredient.Dough;
using DesignPatterns.FactoryPattern.AbstractFactory.Ingredient.Pepperoni;
using DesignPatterns.FactoryPattern.AbstractFactory.Ingredient.Sauce;
using DesignPatterns.FactoryPattern.AbstractFactory.Ingredient.Veggie;

namespace DesignPatterns.FactoryPattern.AbstractFactory
{
    public interface PizzaIngredientFactory
    {
        Dough CreateDough();
        Sauce CreateSauce();
        Cheese CreateCheese();
        Veggies[] CreateVeggies();
        Clams CreateClam();
    }
}