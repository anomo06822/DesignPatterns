namespace DesignPatterns.FactoryPattern.AbstractFactory
{
    public class AbstractFactoryDemo
    {
        public void Start()
        {
            PizzaStore nyStore = new NYPizzaStore();

            Pizza pizza = nyStore.OrderPizza("Cheese");
            System.Console.WriteLine("Ethan ordered a " + pizza.getName() + "\n");

            System.Console.WriteLine("------------------------------------------------");

            pizza = nyStore.OrderPizza("Clam");
            System.Console.WriteLine("Ethan ordered a " + pizza.getName() + "\n");

        }
    }
}