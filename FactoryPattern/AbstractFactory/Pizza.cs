using System.Collections;
using DesignPatterns.FactoryPattern.AbstractFactory.Ingredient.Cheese;
using DesignPatterns.FactoryPattern.AbstractFactory.Ingredient.Clam;
using DesignPatterns.FactoryPattern.AbstractFactory.Ingredient.Dough;
using DesignPatterns.FactoryPattern.AbstractFactory.Ingredient.Pepperoni;
using DesignPatterns.FactoryPattern.AbstractFactory.Ingredient.Sauce;
using DesignPatterns.FactoryPattern.AbstractFactory.Ingredient.Veggie;

namespace DesignPatterns.FactoryPattern.AbstractFactory
{
    public abstract class Pizza
    {
        public string name;
        public Dough dough;
        public Sauce sauce;
        public Veggies[] veggies;
        public Cheese cheese;
        public Pepperoni perpperoni;
        public Clams clam;
        
        public abstract void Prepare();

        public virtual void Bake()
        {
            System.Console.WriteLine("Bake for 25 minutes at 350");
        }

        public virtual void Cut()
        {
            System.Console.WriteLine("Cutting the pizza into diagonal slices");
        }

        public virtual void Box()
        {
            System.Console.WriteLine("Place pizza in official PizzaStore box");
        }

        public void SetName(string name)
        {
            this.name = name;
        }

        public string getName()
        {
            return name;
        }

        public override string ToString()
        {
            return "Show";
        }
    }
}