namespace DesignPatterns.FactoryPattern.SimpleFactory
{
    public class SimpleFactoryDemo
    {
        public void Start()
        {
            SimplePizzaFactory factory = new SimplePizzaFactory();
            PizzaStore store = new PizzaStore(factory);
            store.OrderPizza("Cheese");
            store.OrderPizza("Cheese");
            store.OrderPizza("Veggie");
        }
    }
}