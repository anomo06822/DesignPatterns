namespace DesignPatterns.FactoryPattern.SimpleFactory
{
    public class VeggiePizza : Pizza
    {
        public override void Prepare()
        {
            System.Console.WriteLine("Veggie Prepare");
        }
        public override void Bake()
        {
            System.Console.WriteLine("Veggie Bake");
        }
        public override void Cut()
        {
            System.Console.WriteLine("Veggie Cut");
        }
        public override void Box()
        {
            System.Console.WriteLine("Veggie Box");
        }
    }
}