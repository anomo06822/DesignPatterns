namespace DesignPatterns.FactoryPattern.SimpleFactory
{
    public class CheesePizza : Pizza
    {
        public override void Prepare()
        {
            System.Console.WriteLine("Cheese Prepare");
        }
        public override void Bake()
        {
            System.Console.WriteLine("Cheese Bake");
        }
        public override void Cut()
        {
            System.Console.WriteLine("Cheese Cut");
        }
        public override void Box()
        {
            System.Console.WriteLine("Cheese Box");
        }
    }
}