namespace DesignPatterns.FactoryPattern.SimpleFactory
{
    public class SimplePizzaFactory
    {
        public Pizza CreatePizza(string type)
        {
            Pizza pizza = null;

            if(type.Equals("Cheese"))
            {
                pizza = new CheesePizza();
            }
            else if(type.Equals("Veggie"))
            {
                pizza = new VeggiePizza();
            }

            return pizza;
        }
    }
}