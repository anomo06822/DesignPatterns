namespace DesignPatterns.DecoratorPattern
{
    public abstract class CondimentDecorator : Beverage
    {   
        private Beverage beverage;
        public CondimentDecorator(Beverage beverage)
        {
            this.beverage = beverage;
        }

        public override string GetDescription()
        {
            return this.beverage.GetDescription();
        }

        public override double Cost()
        {
            return this.beverage.Cost();
        }

    }
}