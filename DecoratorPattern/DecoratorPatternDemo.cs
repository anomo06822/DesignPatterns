namespace DesignPatterns.DecoratorPattern
{
    public class DecoratorPatternDemo
    {
        public void Start()
        {
            Beverage beverage = new Espresso();
            System.Console.WriteLine(string.Format("{0} $ {1}",beverage.GetDescription(),beverage.Cost()));

            Beverage beverage2 = new HouseBlend();
            System.Console.WriteLine(string.Format("{0} $ {1}",beverage2.GetDescription(),beverage2.Cost()));
            beverage2 = new Mocha(beverage2);
            beverage2 = new Whip(beverage2);
            beverage2 = new Whip(beverage2);
            System.Console.WriteLine(string.Format("{0} $ {1}",beverage2.GetDescription(),beverage2.Cost()));
        }
    }
}