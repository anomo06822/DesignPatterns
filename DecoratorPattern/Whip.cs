namespace DesignPatterns.DecoratorPattern
{
    public class Whip : CondimentDecorator
    {
        Beverage beverage;
        public Whip(Beverage beverage) : base(beverage)
        {
            this.beverage = beverage;
        }

        public override string GetDescription()
        {
            return this.beverage.GetDescription() + ", Whip";
        }

        public override double Cost()
        {
            return 0.15 + beverage.Cost();
        }
    }
}