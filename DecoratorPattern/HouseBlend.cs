namespace DesignPatterns.DecoratorPattern
{
    public class HouseBlend : Beverage
    {
        public override string GetDescription()
        {
            return "HouseBlend";
        }
        public override double Cost()
        {
            return .89;
        }
    }
}