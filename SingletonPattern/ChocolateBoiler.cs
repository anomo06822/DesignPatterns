namespace DesignPatterns.SingletonPattern
{
    public class ChocolateBoiler
    {
        private bool empty;
        private bool boiled;

        private static ChocolateBoiler uniqeInstance;

        private ChocolateBoiler()
        {
            empty = true;
            boiled = false;
        }

        public static ChocolateBoiler GetInstance()
        {
            if(uniqeInstance == null)
            {
                uniqeInstance = new ChocolateBoiler();
            }

            return uniqeInstance;
        }

        public void Fill()
        {
            System.Console.WriteLine("fill action");

            if(IsEmpty())
            {
                System.Console.WriteLine("chocolate boider is not empty and boil");
                empty = false;
                boiled = false;
            }
        }
        
        public void Drain()
        {
            System.Console.WriteLine("drain action");

            if(!IsEmpty() && IsBoiled())
            {
                System.Console.WriteLine("chocolate boider is empty");
                empty = true;
            }
        }

        public void Boil()
        {
            System.Console.WriteLine("boil action");

            if(!IsEmpty() && !IsBoiled())
            {
                System.Console.WriteLine("chocolate boilder is boil");
                boiled = true;
            }
        }

        public bool IsEmpty()
        {
            return empty;
        }

        public bool IsBoiled()
        {
            return boiled;
        }
    }
}