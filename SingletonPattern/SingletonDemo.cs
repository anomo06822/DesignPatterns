using System.Threading.Tasks;

namespace DesignPatterns.SingletonPattern
{
    public class SingletonDemo
    {
        public void Start()
        {
            ChocolateBoiler chocolateBoiler = ChocolateBoiler.GetInstance();

            chocolateBoiler.Fill();
            chocolateBoiler.Boil();
            chocolateBoiler.Drain();

            Singleton singleton = Singleton.GetInstance();
            
            //針對 async 針對獨體模式的使用需要調整，目前下面未完成。
            //Task<AsyncSingleton> asyncSingleton = AsyncSingleton.GetInstance();
        }
    }
}