using System;
using System.Threading.Tasks;

namespace DesignPatterns.SingletonPattern
{
    public class AsyncSingleton
    {
        private static AsyncSingleton uniqueInstance;

        private AsyncSingleton() 
        {
        }

        public async static Task<AsyncSingleton> GetInstance()
        {
            if (uniqueInstance == null)
            {
                uniqueInstance = await Task.Run(() =>  new AsyncSingleton());
            }
            
            return uniqueInstance;
        }
    }
}