namespace DesignPatterns.AdapterPattern
{
    public class MallardDuck : Duck
    {
        public void Quack()
        {
            System.Console.WriteLine("Quack");
        }

        public void Fly()
        {
            System.Console.WriteLine("I'm flying");
        }
    }
}