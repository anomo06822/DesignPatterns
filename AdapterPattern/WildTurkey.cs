namespace DesignPatterns.AdapterPattern
{
    public class WildTurkey : Turkey
    {
        public void Gobble()
        {
            System.Console.WriteLine("Gobble gobble");
        }

        public void Fly()
        {
            System.Console.WriteLine("I'm flying a short distance");
        }
    }
}