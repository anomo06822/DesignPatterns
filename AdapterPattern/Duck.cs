namespace DesignPatterns.AdapterPattern
{
    public interface Duck
    {
         void Quack();

         void Fly();
    }
}