namespace DesignPatterns.AdapterPattern
{
    public class AdapterPatternDemo
    {
        public void Start()
        {
            MallardDuck duck = new MallardDuck();

            WildTurkey turkey = new WildTurkey();
            Duck turkeyAdapter = new TurkeyAdapter(turkey);

            System.Console.WriteLine("The Turkey says...");
            turkey.Gobble();
            turkey.Fly();

            System.Console.WriteLine("\nThe Duck says...");
            TestDuck(duck);

            System.Console.WriteLine("\nThe TurkeyAdapter says...");
            TestDuck(turkeyAdapter);
        }

        public static void TestDuck(Duck duck)
        {
            duck.Quack();
            duck.Fly();
        }
    }
}