namespace DesignPatterns.AdapterPattern
{
    public interface Turkey
    {
         void Gobble();

         void Fly();
    }
}