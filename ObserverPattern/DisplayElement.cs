namespace DesignPatterns.ObserverPattern
{
    public interface DisplayElement
    {
        void Display();
    }
}