namespace DesignPatterns.ObserverPattern
{
    public class CustomerObserver : Observer, DisplayElement
    {
        private Subject subject;
        private decimal price = 0m;
        public CustomerObserver(Subject subject)
        {
            this.subject = subject;
            this.subject.RegisterObserver(this);
        }
        
        public void Update(decimal price)
        {
            this.price = price;
            Display();
        }

        public void Display()
        {
            System.Console.WriteLine(string.Format("Product price : {0}", this.price));
        }
    }
}