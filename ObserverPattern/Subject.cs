using System.Collections;

namespace DesignPatterns.ObserverPattern
{
    public interface Subject
    {
        void RegisterObserver(Observer observer);
        void RemoveObserver(Observer observer);
        void NotifyObserver();
    }
}