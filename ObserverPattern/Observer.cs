namespace DesignPatterns.ObserverPattern
{
    public interface Observer
    {
        void Update(decimal price);    
    }
}