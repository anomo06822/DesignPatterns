namespace DesignPatterns.ObserverPattern
{
    public class ObserverPatternDemo
    {
        public void Start()
        {
            
            ProductPriceSubject priceSubjet = new ProductPriceSubject();
            CustomerObserver customer = new CustomerObserver(priceSubjet);
            CustomerObserver customer1 = new CustomerObserver(priceSubjet);
            CustomerObserver customer2 = new CustomerObserver(priceSubjet);
            ///subject change price and notify customer observer.
            priceSubjet.SetProductPirce(30);
            priceSubjet.SetProductPirce(50);
            ///subject remove customer observer.
            priceSubjet.RemoveObserver(customer1);
            priceSubjet.SetProductPirce(70);
            priceSubjet.SetProductPirce(70);
        }
    }
}