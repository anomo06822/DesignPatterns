using System.Collections;

namespace DesignPatterns.ObserverPattern
{
    public class ProductPriceSubject : Subject
    {
        private ArrayList observers = new ArrayList();
        private decimal price = 0m;
        private bool changeFlag = false;

        public void RegisterObserver(Observer o)
        {
            int i = observers.IndexOf(o);

            if(i == -1)
            {
                observers.Add(o);
            }
        }

        public void RemoveObserver(Observer o)
        {
            int i = observers.IndexOf(o);

            if(i > 0)
            {
                observers.RemoveAt(i);
            }
        }

        public void NotifyObserver()
        {
            foreach(Observer o in observers)
            {
                o.Update(this.price);
            }
        }

        public void SetProductPirce(decimal currentPrice)
        {
            if(this.price != currentPrice)
            {
                this.changeFlag = true;
                this.price = currentPrice;
            }

            SendCurrentPriceToObservers();
        }
        
        public void SendCurrentPriceToObservers()
        {
            if(this.changeFlag)
            {
                this.NotifyObserver();
                this.changeFlag = false;
            }
        }

        public void SetChangeFlag()
        {
            this.changeFlag = true;
        }
    }
}