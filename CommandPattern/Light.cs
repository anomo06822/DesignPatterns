namespace DesignPatterns.CommandPattern
{
    public class Light
    {
        public void On()
        {
            System.Console.WriteLine("Light is on");
        }
    }
}