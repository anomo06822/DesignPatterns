namespace DesignPatterns.CommandPattern
{
    public class SimpleRemoteControl
    {
        Command command;
        public SimpleRemoteControl(){}

        public void SetCommand(Command command)
        {
            this.command = command;
        }

        public void ButtonWasPressed()
        {
            this.command.Execute();
        }
    }
}