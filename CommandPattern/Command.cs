namespace DesignPatterns.CommandPattern
{
    public interface Command
    {
         void Execute();
    }
}