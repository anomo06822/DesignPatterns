namespace DesignPatterns.CommandPattern
{
    public class SimpleRemoteControlDemo
    {
        public void Start()
        {
            SimpleRemoteControl remote = new SimpleRemoteControl();
            Light light = new Light();
            LightOnCommand lightOn = new LightOnCommand(light);

            remote.SetCommand(lightOn);
            remote.ButtonWasPressed();
        }
        
    }
}